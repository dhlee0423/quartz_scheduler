package kr.stam.batch.dto;

import lombok.Data;

@Data
public class CronTriggerDTO {

	private String triggerName;
	
	private String triggerGroup;
	
	private String cronExpression;
	
}
