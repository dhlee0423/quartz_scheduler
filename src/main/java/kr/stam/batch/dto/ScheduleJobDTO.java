package kr.stam.batch.dto;

import lombok.Data;

@Data
public class ScheduleJobDTO {

	private String schedName;
	
	private String jobName;
	
	private String jobGroup;
	
	private String description;
	
	private String jobClassName;
	
	private String isDurable;
	
	private String isNonConcurrent;
	
	private String isUpdateData;
	
	private String requestsRecovery;
	
	private CronTriggerDTO cronTriggerDTO;
}
