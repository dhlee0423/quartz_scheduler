package kr.stam.batch.configuration;

import javax.sql.DataSource;

import org.quartz.SchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
public class QuartzConfiguration {

	@Autowired
	private DataSource dataSource;
	
    @Bean
    public SchedulerFactoryBean schedulerFactory() {
    	SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
    	schedulerFactoryBean.setDataSource(dataSource);
    	schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContext");
    	
    	return schedulerFactoryBean;
    }
}
