package kr.stam.batch.scheduler;

import lombok.Getter;

@Getter
public enum BatchEnum {
	
	// Batch 잡 리스트
	TEST_JOB("Exam1", "ExamGroup", "description", "kr.stam.batch.job.TestJob", "0/10 * * * * ?"),
	
	TEST_JOB2("Exam2", "ExamGroup", "description", "kr.stam.batch.job.TestJob2", "0/5 * * * * ?");
	
	private String jobName;
	
	private String groupName;
	
	private String description;
	
	private String jobClassName;
	
	private String cronExpression;
		
	BatchEnum(String jobName, String groupName, String description, String jobClassName, String cronExpression) {
		this.jobName = jobName;
		this.groupName = groupName;
		this.description = description;
		this.jobClassName = jobClassName;
		this.cronExpression = cronExpression;
	}	
}
