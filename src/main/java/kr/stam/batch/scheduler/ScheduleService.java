package kr.stam.batch.scheduler;

import java.util.List;
import java.util.Map;

import org.quartz.JobKey;
import org.quartz.utils.DirtyFlagMap;

import kr.stam.batch.dto.ScheduleJobDTO;

public interface ScheduleService {

	public Map<JobKey, Object> selectScheduleMap();

	public List selectSchedule(Map query);

	public ScheduleJobDTO selectSchedule(String jobName, String jobGroup);

}
