package kr.stam.batch.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import kr.stam.batch.dto.CronTriggerDTO;
import kr.stam.batch.dto.ScheduleJobDTO;

@Service
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	ScheduleMapper scheduleMapper;
	
	@Override
	public Map<JobKey, Object> selectScheduleMap() {
		
		List<ScheduleJobDTO> jobList = scheduleMapper.selectSchedule(null);
		
		HashMap<JobKey, Object> map = new HashMap<>();
		
		// jobName을 key로 갖도록 DTO를 put 한다.
		for(ScheduleJobDTO dto : (ArrayList<ScheduleJobDTO>) jobList) {
			String jobName = dto.getJobName();
			String jobGroup = dto.getJobGroup();
			if(StringUtils.hasText(jobName)) map.put(new JobKey(jobName, jobGroup), dto);
		}
		
		return map;
	}
	
	@Override
	public List selectSchedule(Map query) {
		
		List list = scheduleMapper.selectSchedule(query); 
		
		for(ScheduleJobDTO job : (ArrayList<ScheduleJobDTO>)list) {
			Map triggerQuery = new HashMap<String, String>();
			triggerQuery.put("triggerName", job.getJobName());
			triggerQuery.put("triggerGroup", job.getJobGroup());
			
			CronTriggerDTO cronTrigger = scheduleMapper.selectCronTrigger(triggerQuery); 
			
			job.setCronTriggerDTO(cronTrigger);
		}
		
		return list;
	}

	@Override
	public ScheduleJobDTO selectSchedule(String jobName, String jobGroup) {
		Map query = new HashMap<>();
		query.put("jobName", jobName);
		query.put("jobGroup", jobGroup);
		
		ScheduleJobDTO job = null;
		List jobList = selectSchedule(query);
		if(jobList.size() == 1) {
			job = (ScheduleJobDTO) jobList.get(0);
		}
		
		return job;
	}
}
