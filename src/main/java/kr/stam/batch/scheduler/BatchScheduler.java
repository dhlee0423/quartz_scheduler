package kr.stam.batch.scheduler;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import kr.stam.batch.dto.ScheduleJobDTO;

import static org.quartz.JobBuilder.newJob;

import java.util.HashMap;
import java.util.Map;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobDataMap;

@Component
public class BatchScheduler implements ApplicationRunner {
	
	@Autowired
	private Scheduler scheduler;
	
	@Autowired
	private ScheduleService scheduleService;
	
	/**
	 * ApplicationRunner의 필수 구현체로서 Spring app이 실행될 때 아래의 메서드를 수행하도록 로직을 구성.
	 * Scheduler
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		// 배치잡 Select 및 Schedule 등록
		initScheduler();
		
		scheduler.start();
	}
		
	private void initScheduler() throws SchedulerException, ClassNotFoundException {
		
		// Enum 기준으로 Job Schedule을 동기화
		
		// Step 1. 현재 DB에 등록된 Job 목록을 Map으로서 획득
		Map<JobKey, Object> notExistJobMap = scheduleService.selectScheduleMap();
		
		// Step 2. Enum에 작성된 Job을 순차적으로 조회
		for(BatchEnum batch : BatchEnum.values()) {			
			
			JobKey jobKey = new JobKey(batch.getJobName(), batch.getGroupName());
			
			// Step 3. Enum에 작성되어있으나 DB에는 정보가 없다면 Scheduler에 등록
			if(!scheduler.checkExists(jobKey)) {
				
				JobDataMap map = new JobDataMap();
				JobDetail jdt = newJob(getClass(batch.getJobClassName()))
				.withIdentity(batch.getJobName(), batch.getGroupName())
				.withDescription(batch.getDescription())
				.storeDurably(true)
				.usingJobData(map)
				.build();
		
				Trigger trigger = TriggerBuilder.newTrigger()
				.withIdentity(batch.getJobName(), batch.getGroupName())
				.withDescription(batch.getDescription())
				.withSchedule(CronScheduleBuilder.cronSchedule(batch.getCronExpression())).build();
				
				scheduler.scheduleJob(jdt, trigger);
			} else {
				// Step 4. DB와 Enum에 모두 존재할 경우 삭제 대상에서 제외
				notExistJobMap.remove(jobKey);
			}
		}
		
		// Step 5. DB에는 정보가 있으나 Enum에는 정보가 없는 Job은 Scheduler에서 제외
		for(JobKey jobKey : notExistJobMap.keySet()) {
			scheduler.deleteJob(jobKey);
		}
		
	}	

	private Trigger buildJobTrigger(String jobName, String jobGroup, String cronExp) {
		return TriggerBuilder.newTrigger()
				.withIdentity(jobName)
				.withSchedule(CronScheduleBuilder.cronSchedule(cronExp)).build();
	}

	private Class<? extends Job> getClass(String jobClassName) throws ClassNotFoundException {
		return (Class <? extends Job>) Class.forName(jobClassName);
	}

	/**
	 * Job을 중단한다. 중단된 job은 Server가 startup되어도 자동으로 시작되지 않는다.
	 * @param jobName
	 * @param jobGroup
	 * @return
	 * @throws SchedulerException
	 */
	public void pause(String jobName, String jobGroup) throws SchedulerException {
				
		ScheduleJobDTO job = scheduleService.selectSchedule(jobName, jobGroup);
		
		scheduler.pauseJob(new JobKey(job.getJobName(), job.getJobGroup()));
	}
	
	/**
	 * Job을 재시작한다.
	 * @param jobName
	 * @param jobGroup
	 * @return
	 * @throws SchedulerException 
	 */
	public void resume(String jobName, String jobGroup) throws SchedulerException {
		
		ScheduleJobDTO job = scheduleService.selectSchedule(jobName, jobGroup);
		
		scheduler.resumeJob(new JobKey(job.getJobName(), job.getJobGroup()));
	}
	
	/**
	 * Job의 다음 Schedule을 변경한다.
	 * @param jobName
	 * @param jobGroup
	 * @param cronExp
	 * @return
	 * @throws SchedulerException 
	 */
	public void reschedule(String jobName, String jobGroup, String cronExp) throws SchedulerException {
		
		ScheduleJobDTO job = scheduleService.selectSchedule(jobName, jobGroup);
		
		TriggerKey key = new TriggerKey(job.getJobName(), job.getJobGroup());
		
		Trigger updateTrigger = buildJobTrigger(jobName, jobGroup, cronExp);
	
		scheduler.rescheduleJob(key, updateTrigger);
	}
}
 