package kr.stam.batch.scheduler;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import kr.stam.batch.dto.CronTriggerDTO;
import kr.stam.batch.dto.ScheduleJobDTO;

@Mapper
public interface ScheduleMapper {

	public List<ScheduleJobDTO> selectSchedule(Map query);

	public CronTriggerDTO selectCronTrigger(Map query);	
}
