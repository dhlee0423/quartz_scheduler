package kr.stam.batch.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import kr.stam.batch.scheduler.ScheduleService;

/*
 * Batch job 예시 
 */
public class TestJob extends QuartzJobBean {	

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		System.out.println("its working :)");
	}
}
