package kr.stam.batch.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import kr.stam.batch.scheduler.BatchScheduler;
import kr.stam.batch.scheduler.ScheduleService;

@Controller
public class BatchController {
	
	@Autowired
	ScheduleService scheduleService;
	
	@Autowired
	BatchScheduler batchScheduler;
	
	@RequestMapping("view")
	public String viewBatch(Model model) {
		
		List batchList = scheduleService.selectSchedule(null);
		
		model.addAttribute("batchList", batchList);
		
		return "view";
	}
	
	@PostMapping("/batch/pause")
	public ResponseEntity pauseBatch(@RequestBody Map<String, Object> query) throws SchedulerException {
		HashMap<String, Object> resultMsg = new HashMap<>();
		resultMsg.put("RESULT", false);
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		
		String jobName = (String) query.get("jobName");
		String jobGroup = (String) query.get("jobGroup");
		try {
			batchScheduler.pause(jobName, jobGroup);
			httpStatus = HttpStatus.OK;
			resultMsg.put("RESULT", true);
		} catch (Exception e) {
			// Error Logging
			resultMsg.put("ERROR_MESSAGE", e);
		}
		
		return new ResponseEntity(resultMsg, httpStatus);
	}
	
	@PostMapping("/batch/resume")
	public ResponseEntity resumeBatch(@RequestBody Map<String, Object> query) throws SchedulerException {
		HashMap<String, Object> resultMsg = new HashMap<>();
		resultMsg.put("RESULT", false);
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		
		String jobName = (String) query.get("jobName");
		String jobGroup = (String) query.get("jobGroup");
		
		try {
			batchScheduler.resume(jobName, jobGroup);
			httpStatus = HttpStatus.OK;
			resultMsg.put("RESULT", true);
		} catch (Exception e) {
			// Error Logging
			resultMsg.put("ERROR_MESSAGE", e);
		}
		
		return new ResponseEntity(resultMsg, httpStatus);
	}
	
	@PostMapping("/batch/reschedule")
	public ResponseEntity rescheduleBatch(@RequestBody Map<String, Object> query) throws SchedulerException {
		HashMap<String, Object> resultMsg = new HashMap<>();
		resultMsg.put("RESULT", false);
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		
		String jobName = (String) query.get("jobName");
		String jobGroup = (String) query.get("jobGroup");
		String cronExp = (String) query.get("cronExp");
		
		try {
			batchScheduler.reschedule(jobName, jobGroup, cronExp);
			httpStatus = HttpStatus.OK;
			resultMsg.put("RESULT", true);
		} catch (Exception e) {
			// Error Logging
			resultMsg.put("ERROR_MESSAGE", e);
		}
		
		return new ResponseEntity(resultMsg, httpStatus);
	}
}
